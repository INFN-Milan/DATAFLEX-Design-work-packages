This Project folder contains every document
and project file needed to produce and debug
the Dataflex-S_G4.1 PCB; including 3D files,
Hyperlinx stackup simulations, GERBERs and NC
Drill files.

The scope of the Dataflex project is to 
interconnect a backplane board, mated with the
Megarray400 connector, to little boards wire
bonded and placed where the interfaces P1,P2 and P3
are placed on the PCB.

It's very important to have 100 Ohm impedance on
layer Top and Bottom (differential pairs), and to
reduce the material budget as much as possible.

The PCB will be exposed up to 40Mrad of integrated
radiation.